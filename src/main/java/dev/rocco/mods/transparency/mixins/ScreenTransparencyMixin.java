package dev.rocco.mods.transparency.mixins;

import dev.rocco.mods.transparency.TransparencyMod;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Screen.class)
public abstract class ScreenTransparencyMixin {
    @Inject(method = "renderBackground(I)V", at = @At("HEAD"), cancellable = true)
    public void dontRender(int bg, CallbackInfo ci) {
        if(TransparencyMod.TRANSPARENT_GUI && bg == 0 && MinecraftClient.getInstance().world != null) {
            ci.cancel();
        }
    }
}

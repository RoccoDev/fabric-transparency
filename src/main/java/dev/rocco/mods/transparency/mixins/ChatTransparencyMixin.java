package dev.rocco.mods.transparency.mixins;

import dev.rocco.mods.transparency.TransparencyMod;
import net.minecraft.client.gui.hud.ChatHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

@Mixin(ChatHud.class)
public abstract class ChatTransparencyMixin {
    @ModifyArg(method = "render", at = @At(value = "INVOKE", ordinal = 0,
            target = "net/minecraft/client/gui/hud/ChatHud.fill(IIIII)V"), index = 4)
    public int customAlpha(int previous) {
        return TransparencyMod.TRANSPARENT_CHAT ? 0 : previous;
    }
}

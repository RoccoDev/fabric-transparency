package dev.rocco.mods.transparency;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.CommandRegistry;
import net.fabricmc.loader.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.command.CommandManager;

import java.io.*;
import java.util.Properties;

public class TransparencyMod implements ModInitializer {

    public static boolean TRANSPARENT_CHAT, TRANSPARENT_GUI;

    @Override
    public void onInitialize() {
        File configFile = new File(FabricLoader.INSTANCE.getConfigDirectory(), "transparency.properties");
        Properties props = new Properties();
        if(!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            TRANSPARENT_GUI = true;
            TRANSPARENT_CHAT = true;

            save();
        }
        else {
            try(InputStream is = new FileInputStream(configFile)) {
                props.load(is);

                TRANSPARENT_CHAT = Boolean.parseBoolean(props.getProperty("chat", "true"));
                TRANSPARENT_GUI = Boolean.parseBoolean(props.getProperty("gui", "true"));
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }


        CommandRegistry.INSTANCE.register(false, dispatcher ->
                dispatcher.register(CommandManager.literal("transparency")
                        .executes(ctx -> {
                            MinecraftClient.getInstance().openScreen(new ConfigGui());
                            return 1;
                        })));
    }

    public static void save() {
        File configFile = new File(FabricLoader.INSTANCE.getConfigDirectory(), "transparency.properties");
        Properties props = new Properties();
        props.put("chat", Boolean.toString(TRANSPARENT_CHAT));
        props.put("gui", Boolean.toString(TRANSPARENT_GUI));

        try(OutputStream os = new FileOutputStream(configFile)) {
            props.store(os, null);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

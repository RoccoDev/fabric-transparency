package dev.rocco.mods.transparency;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.LiteralText;

public class ConfigGui extends Screen {

    ConfigGui() {
        super(new LiteralText("Transparency Settings"));
    }

    @Override
    protected void init() {
        addButton(new ButtonWidget(width / 2 - 60, height / 2 - 10, 120, 20, "Chat: " +
                (TransparencyMod.TRANSPARENT_CHAT ? "Transparent" : "Normal"), buttonWidget -> {
                    TransparencyMod.TRANSPARENT_CHAT = !TransparencyMod.TRANSPARENT_CHAT;
                    buttonWidget.setMessage("Chat: " + (TransparencyMod.TRANSPARENT_CHAT ? "Transparent" : "Normal"));
                }));

        addButton(new ButtonWidget(width / 2 - 60, height / 2 + 15, 120, 20, "Menus: " +
                (TransparencyMod.TRANSPARENT_GUI ? "Transparent" : "Normal"), buttonWidget -> {
            TransparencyMod.TRANSPARENT_GUI = !TransparencyMod.TRANSPARENT_GUI;
            buttonWidget.setMessage("Menus: " + (TransparencyMod.TRANSPARENT_GUI ? "Transparent" : "Normal"));
        }));
    }

    @Override
    public void render(int int_1, int int_2, float float_1) {
        renderBackground();
        super.render(int_1, int_2, float_1);
    }

    @Override
    public void onClose() {
        TransparencyMod.save();
        super.onClose();
    }
}
